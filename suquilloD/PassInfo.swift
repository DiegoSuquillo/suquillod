//
//  PassInfo.swift
//  suquilloD
//
//  Created by Jaime Pinto Cortez on 6/6/18.
//  Copyright © 2018 epn. All rights reserved.
//

import Foundation

struct passInfo {
    let name:String
    let nextLink:String
}
