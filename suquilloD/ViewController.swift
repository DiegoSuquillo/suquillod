//
//  ViewController.swift
//  suquilloD
//
//  Created by Ronny Cabrera on 5/6/18.
//  Copyright © 2018 epn. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameInputText: UITextField!
   
    @IBAction func nextPressedButton(_ sender: Any) {
        let urlString =  "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let group = DispatchGroup()
        
        group.enter()
        
        let task = session.dataTask(with: url!) {
            data, response, error in
            
            guard let data = data else {
                print("Error NO data")
                return
            }
            
            guard let sourceInfo = try? JSONDecoder().decode(DecodeInfo.self, from: data) else {
                print("Error Decoding")
                return
            }
            
            DispatchQueue.main.async {
                self.titleLabel.text = "\(sourceInfo.viewTitle)"
                self.dateLabel.text = "\(sourceInfo.date)"
            }
            
            group.leave()
        }
        task.resume()
        group.wait()
        performSegue(withIdentifier: "toNextViewSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let resultData = passInfo(name: nameInputText.text!, nextLink: "https://api.myjson.com/bins/xsm5e")
        if segue.identifier == "toNextViewSegue" {
            let destination = segue.destination as! ViewItemsController
            destination.dataInfo = resultData
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

