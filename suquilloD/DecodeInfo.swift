//
//  DecodeInfo.swift
//  suquilloD
//
//  Created by Ronny Cabrera on 5/6/18.
//  Copyright © 2018 epn. All rights reserved.
//

import Foundation

struct DecodeInfo: Decodable {
    let viewTitle:String
    let date:String
    let nextLink:String
}
