//
//  ViewItemsController.swift
//  suquilloD
//
//  Created by Jaime Pinto Cortez on 6/6/18.
//  Copyright © 2018 epn. All rights reserved.
//

import UIKit

class ViewItemsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var resultLabel: UILabel!
    var dataInfo:passInfo?
    var datos:[dataTable] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let urlString = "https://api.myjson.com/bins/182sje"
        let url = URL(string: (urlString))
        let session = URLSession.shared

        let task = session.dataTask(with: url!) {
            data, response, error in
            
            guard let data = data else {
                print("Error NO data")
                return
            }
            
            guard let sourceInfo = try? JSONDecoder().decode(table.self, from: data) else {
                print("Error Decoding")
                return
            }
            
            DispatchQueue.main.async {
                self.datos = sourceInfo.data
                print(self.datos)
            }
        }
        task.resume()
        
        print(self.datos)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return  section == 0 ? dataInfo?.name : ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell") as! ItemTableViewCell
        if indexPath.section == 0 {
                cell.nameLabel?.text = self.datos[0].label
                cell.valueLabel?.text = "\(self.datos[0].value)"
                
        }
       
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
