//
//  SecondInfo.swift
//  suquilloD
//
//  Created by Jaime Pinto Cortez on 6/6/18.
//  Copyright © 2018 epn. All rights reserved.
//

import Foundation

struct table: Decodable {
    let data: [dataTable]
}

struct dataTable: Decodable {
    let label: String
    let value: Int
}
